package com.system.appremove.activites;

import android.graphics.Color;
import android.os.Bundle;

import com.system.appremove.R;
import com.vlonjatg.android.apptourlibrary.MaterialSlide;

/**
 * Created by Aman on 9/5/2015.
 */
public class AppTour extends com.vlonjatg.android.apptourlibrary.AppTour{
    @Override
    public void init(Bundle bundle) {
        addSlide(MaterialSlide.newInstance(R.drawable.ic_next_white_24dp,"Slide 1","Slide icfo", Color.WHITE,Color.WHITE),getResources().getColor(R.color.COLOR_1));
        addSlide(MaterialSlide.newInstance(R.drawable.ic_next_white_24dp,"Slide 2","Slide icfo", Color.WHITE,Color.WHITE),getResources().getColor(R.color.COLOR_2));
        addSlide(MaterialSlide.newInstance(R.drawable.ic_next_white_24dp,"Slide 3","Slide icfo", Color.WHITE,Color.WHITE),getResources().getColor(R.color.COLOR_3));
        addSlide(MaterialSlide.newInstance(R.drawable.ic_next_white_24dp,"Slide 4","Slide icfo", Color.WHITE,Color.WHITE),getResources().getColor(R.color.COLOR_4));
        addSlide(MaterialSlide.newInstance(R.drawable.ic_next_white_24dp,"Slide 5","Slide icfo", Color.WHITE,Color.WHITE),getResources().getColor(R.color.COLOR_5));
        setSkipButtonTextColor(Color.WHITE);
        setNextButtonColorToWhite();
        setDoneButtonTextColor(Color.WHITE);

    }

    @Override
    public void onSkipPressed() {
        finish();
    }

    @Override
    public void onDonePressed() {
        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
