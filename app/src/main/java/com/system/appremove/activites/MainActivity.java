package com.system.appremove.activites;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.system.appremove.Dialog.RateThisApp;
import com.system.appremove.R;
import com.system.appremove.Tool.Logs;
import com.system.appremove.data.Keys;
import com.system.appremove.data.PoJo;
import com.system.appremove.model.Customadapter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static PackageManager packageManager = null;
    private static List<ApplicationInfo> applist = null;
    private static ListView listView;
    private SearchView searchview;
    private static ProgressBar progress;
    private static boolean isBusy = false;
    private static boolean includeSystemApps = true;
    private long backback;
    public static boolean Longclicked = false;
    private static Context context;
    private static SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this,AppTour.class));
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(getResources().getColor(R.color.theme_color)));
        packageManager = getPackageManager();
        listView = (ListView) findViewById(R.id.expandableListView1);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipetorefresh);
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE);
        swipeRefreshLayout.setOnRefreshListener(this);
        listView.addFooterView(new View(this), null, false);
        listView.addHeaderView(new View(this), null, false);
        progress = (ProgressBar) findViewById(R.id.progresslist);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                new LoadApplications(false, "").execute();
            }
        });


        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int pos,
                                    long id2) {
                Logs.d("System App = " + PoJo.getListdata().get((int) id2).get(Keys.HASHMAP_KEY_SYSTEM));
                Logs.d("App Friezed = " + PoJo.getListdata().get((int) id2).get(Keys.HASHMAP_KEY_DISABLED));


            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_systemapps).setChecked(true);
        MenuItem searchItem = menu.findItem(R.id.action_searchview);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchview = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchview.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchview.setSubmitButtonEnabled(true);
        searchview.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String arg0) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {
                Updatelist(arg0);
                return false;
            }

        });
        return true;
    }

    private void Updatelist(String query) {
        Logs.d(query);
        if (TextUtils.isEmpty(query)) {
            if (!isBusy)
                new LoadApplications(false, "").execute();
            return;
        }
        if (!isBusy) {
            String system = Character.toUpperCase(query.charAt(0))
                    + query.substring(1, query.length());
            new LoadApplications(true, system).execute();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_sort:
                View menuItemView = findViewById(R.id.action_sort);
                PopupMenu popupMenu = new PopupMenu(this, menuItemView);
                popupMenu.inflate(R.menu.menu_sort);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.sort_action_a2z:
                                PoJo.SORT_TYPE = PoJo.SORT_A2Z;
                                break;
                            case R.id.sort_action_z2a:
                                PoJo.SORT_TYPE = PoJo.SORT_Z2A;
                                break;
                            case R.id.sort_action_installdate:
                                PoJo.SORT_TYPE = PoJo.SORT_DATE;
                                break;
                            case R.id.sort_action_size:
                                PoJo.SORT_TYPE = PoJo.SORT_SIZE;
                                break;

                        }
                        new LoadApplications(false, "").execute();
                        return true;
                    }
                });
                popupMenu.show();
                break;
            case R.id.action_systemapps:
                if (includeSystemApps) {
                    item.setChecked(false);
                    includeSystemApps = false;
                } else {
                    item.setChecked(true);
                    includeSystemApps = true;
                }
                new LoadApplications(false, "").execute();
                break;

            case R.id.action_clearcache:
                String path = Environment.getExternalStorageDirectory().toString();
                File dir = new File(path, "/"
                        + getResources().getString(R.string.app_name) + "/.temp");
                if (dir.exists()) {
                    String Size = null;
                    long sizeinbytes = 0;
                    String[] files = dir.list();
                    for (int i = 0; i < files.length; i++) {
                        File file = new File(dir + "/" + files[i]);
                        sizeinbytes = sizeinbytes + file.length();
                        file.delete();
                    }
                    Size = String.valueOf((double) ((sizeinbytes / 1024) / 1024));
                    if (sizeinbytes == 0)
                        Toast.makeText(getApplicationContext(), "Nothing to clear",
                                Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getApplicationContext(),
                                Size + " Mb Cleared", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "No Cache found",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.action_rate:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + getPackageName())));

                } catch (Exception e) {
                    startActivity(new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id="
                                    + getPackageName())));
                }
                break;
            case R.id.action_share:
                Intent i1 = new Intent(Intent.ACTION_SEND);
                i1.setType("text/plain");
                i1.putExtra(android.content.Intent.EXTRA_SUBJECT,
                        "I Suggest An App For you. Have A Look\n");
                i1.putExtra(
                        android.content.Intent.EXTRA_TEXT,
                        "Hey, Check This App to Delete/Remove Pre-installed App from your Phone and Make Space.\nYou Can backup,Share,Disable those apps.\nThis App is rich of many Unique features\nGet this App from Here.\nhttps://play.google.com/store/apps/details?id="
                                + getPackageName());
                i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        new LoadApplications(false, "").execute();
    }

    private static class LoadApplications extends AsyncTask<Void, Void, Void> {
        Boolean filter;
        String query;
        long START_TIME, STOP_TIME, FIX_DELAY = 0; //FIXED MILISECOND TO LOAD DATA

        public LoadApplications(Boolean filter, String query) {
            this.filter = filter;
            this.query = query;
        }

        @Override
        protected void onPreExecute() {
            START_TIME = System.currentTimeMillis();
            progress.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(true);
            isBusy = true;
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            applist = checkForLaunchIntent(packageManager
                    .getInstalledApplications(PackageManager.GET_META_DATA));
            ArrayList<HashMap<String, Object>> lsitdata = new ArrayList<>();

            HashMap<String, Object> data;
            String Dir = "";
            boolean isSystemapp;
            for (int i = 0; i < applist.size(); i++) {
                data = new HashMap<>();
                isSystemapp = (applist.get(i).flags & ApplicationInfo.FLAG_SYSTEM) != 0;
                if (isSystemapp) {
                    if (!includeSystemApps)
                        continue;
                    Dir = applist.get(i).sourceDir;
                    Logs.d("" + applist.get(i).sourceDir);
                } else {
                    Dir = applist.get(i).sourceDir;
                }
                String AppName = applist.get(i).loadLabel(packageManager)
                        .toString();
                AppName = toTitleCase(AppName);
                String AppPackage = applist.get(i).packageName;
                String AppVersion;
                long installtime;
                long apksize;
                try {
                    AppVersion = packageManager.getPackageInfo(
                            applist.get(i).packageName, 0).versionName;
                    installtime = packageManager.getPackageInfo(applist.get(i).packageName, 0).firstInstallTime;
                    apksize = new File(applist.get(i).sourceDir).length();
                } catch (NameNotFoundException e) {
                    AppVersion = "1.0.0";
                    installtime = new File(applist.get(i).sourceDir).lastModified();
                    apksize = 0;
                    e.printStackTrace();
                }
                if (filter && !TextUtils.isEmpty(query)) {
                    if (AppName.startsWith(query)) {
                        data.put(Keys.HASHMAP_KEY_APPNAME, AppName);
                        data.put(Keys.HASHMAP_KEY_PACKAGE, AppPackage);
                        data.put(Keys.HASHMAP_KEY_VERSION, AppVersion);
                        data.put(Keys.HASHMAP_KEY_APPPATH, Dir);
                        data.put(Keys.HASHMAP_KEY_SYSTEM, isSystemapp);
                        data.put(Keys.HASHMAP_KEY_FILESIZE, apksize);
                        data.put(Keys.HASHMAP_KEY_DISABLED, !applist.get(i).enabled);
                        data.put(Keys.HASHMAP_KEY_INSTALLDATE, installtime);
                        data.put(Keys.HASHMAP_KEY_ICON, applist.get(i).loadIcon(packageManager));
                        lsitdata.add(data);
                    }
                } else {
                    data.put(Keys.HASHMAP_KEY_APPNAME, AppName);
                    data.put(Keys.HASHMAP_KEY_PACKAGE, AppPackage);
                    data.put(Keys.HASHMAP_KEY_VERSION, AppVersion);
                    data.put(Keys.HASHMAP_KEY_APPPATH, Dir);
                    data.put(Keys.HASHMAP_KEY_SYSTEM, isSystemapp);
                    data.put(Keys.HASHMAP_KEY_FILESIZE, apksize);
                    data.put(Keys.HASHMAP_KEY_DISABLED, !applist.get(i).enabled);
                    data.put(Keys.HASHMAP_KEY_INSTALLDATE, installtime);
                    data.put(Keys.HASHMAP_KEY_ICON, applist.get(i).loadIcon(packageManager));
                    lsitdata.add(data);
                }
            }
            PoJo.setListdata(lsitdata);
            STOP_TIME = System.currentTimeMillis();
            if (STOP_TIME - START_TIME < FIX_DELAY) {
                try {
                    Thread.sleep(FIX_DELAY - (STOP_TIME - START_TIME));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            progress.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            int index = listView.getFirstVisiblePosition();
            View v = listView.getChildAt(0);
            int top = (v == null) ? 0 : (v.getTop() - listView.getPaddingTop());
            listView.setAdapter(new Customadapter(context));
            listView.setSelectionFromTop(index, top);
            isBusy = false;
            swipeRefreshLayout.setRefreshing(false);
            super.onPostExecute(result);
        }

        private List<ApplicationInfo> checkForLaunchIntent(
                List<ApplicationInfo> list) {
            ArrayList<ApplicationInfo> applist = new ArrayList<>();
            for (ApplicationInfo info : list) {
                try {
                    if (null != packageManager
                            .getLaunchIntentForPackage(info.packageName)) {
                        applist.add(info);
                    } else {
                        applist.add(info);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return applist;
        }
    }

    @Override
    public void onBackPressed() {
        if (backback + 2000 > System.currentTimeMillis())
            super.onBackPressed();
        else {
            Toast.makeText(getApplicationContext(), "Tap Once More to Exit",
                    Toast.LENGTH_SHORT).show();
            backback = System.currentTimeMillis();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        RateThisApp.onStart(this);
        RateThisApp.showRateDialogIfNeeded(this);
    }

    public static void RefreshList() {
        try {
            new LoadApplications(false, "").execute();
        } catch (Exception e) {
            e.printStackTrace();
            Logs.d("Fuck Exception - " + e.toString());
        }
    }
    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }
}
