package com.system.appremove.activites;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.system.appremove.R;
import com.system.appremove.Tool.Logs;
import com.system.appremove.Tool.Share;
import com.system.appremove.Tool.Tools;
import com.system.appremove.data.Keys;
import com.system.appremove.data.PoJo;


public class Appinfo extends AppCompatActivity {
    public static final String INTENT_KEY_POSITION = "pos";
    private ActionBar actionBar;
    private ImageView Appicon;
    private TextView Appname, AppPackageName, AppVersion, AppSize;
    private int Position;
    private Typeface Main, Other;
    private ListView listView;
    private Tools tools;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appinfo);
        init();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.theme_color)));
        actionBar.setTitle(PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_APPNAME).toString());
        Appicon.setImageDrawable((Drawable) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_ICON));
      //  Appname.setText(PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_APPNAME).toString());
        Appname.setVisibility(View.GONE);
        AppPackageName.setText(PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_PACKAGE).toString());
        AppVersion.setText("Version: " + PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_VERSION));
        AppSize.setText("Apk Size: " + getAppsize() + " MB");

    }

    private void init() {
        actionBar = getSupportActionBar();
        Position = getIntent().getIntExtra(INTENT_KEY_POSITION, 0);
        Appicon = (ImageView) findViewById(R.id.appinfo_imageview_appicon);
        Appname = (TextView) findViewById(R.id.appinfo_textview_appname);
        AppPackageName = (TextView) findViewById(R.id.appinfo_textview_packagename);
        AppVersion = (TextView) findViewById(R.id.appinfo_textview_version);
        AppSize = (TextView) findViewById(R.id.appinfo_textview_size);
        Main = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        Other = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
        Appname.setTypeface(Main);
        AppPackageName.setTypeface(Main);
        AppVersion.setTypeface(Other);
        AppSize.setTypeface(Other);
        listView = (ListView) findViewById(R.id.appinfo_list);
        listView.setAdapter(new AppOptionsAdapter());
        tools = new Tools(this);
    }

    private String getAppsize() {
        Double size = (double) Share.getfilesize(
                PoJo.getListdata().get(Position)
                        .get(Keys.HASHMAP_KEY_PACKAGE).toString(),
                PoJo.getListdata().get(Position)
                        .get(Keys.HASHMAP_KEY_APPPATH).toString());
        size = (size / 1024) / 1024;
        String size2 = size.toString();
        return size2.substring(0, (size2.indexOf(".") + 2));
    }

    class AppOptionsAdapter extends BaseAdapter {
        LayoutInflater inflater;

        String[] headers = {"Share", "Backup", "Remove", "", "Start", "Play Store", "Make Shortcut", "System Info"};
        String[] summery = {"Share this App right NOW!", "Create a Copy of Apk file in sdcard", "Remove/Delete this App Quickly", "", "Start/Open this App Right Now", "View this app on Play Store(If Available", "Create a Shortcut of this App on Home Launcher Screen", "View System info Of this App"};

        public AppOptionsAdapter() {
            if ((boolean) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_DISABLED)) {
                headers[3] = "Enable";
                summery[3] = "Enable this App NOW";
            } else {
                headers[3] = "Disable";
                summery[3] = "Disable this App NOW";
            }

            inflater = LayoutInflater.from(Appinfo.this);
        }

        @Override
        public int getCount() {
            return headers.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.appinfo_list_item, null);
                holder.header = (TextView) convertView
                        .findViewById(R.id.appinfo_list_item_text1);
                holder.summry = (TextView) convertView
                        .findViewById(R.id.appinfo_list_item_text2);
                holder.cardView = (CardView) convertView.findViewById(R.id.cardview);
                holder.header.setTypeface(Main);
                holder.summry.setTypeface(Other);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.header.setText(headers[position]);
            holder.summry.setText(summery[position]);
            if (position == 2)
                holder.header.setTextColor(Color.RED);
            else {
                holder.header.setTextColor(Color.BLACK);
            }
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (position) {
                        case 0:
                            tools.Share(Position);
                            break;
                        case 1:
                            tools.backup(Position);
                            break;
                        case 2:
                            tools.Uninstall(Position);
                            break;
                        case 3:
                            tools.EnableDisable(Position);
                            break;
                        case 4:
                            tools.Start(Position);
                            break;
                        case 5:
                            tools.googleplay(Position);
                            break;
                        case 6:
                            tools.Shortcut(Position);
                            break;
                        case 7:
                            tools.Moreinfo(Position);
                            break;

                    }
                }
            });
            return convertView;
        }

        public class ViewHolder {
            TextView header, summry;
            CardView cardView;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Keys.START_ACTIVITY_KEY_UNINSTALL:
                Logs.d("From Uninstall");
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
