package com.system.appremove.data;

public class Keys {
    public static String HASHMAP_KEY_APPNAME = "q";
    public static String HASHMAP_KEY_PACKAGE = "w";
    public static String HASHMAP_KEY_VERSION = "e";
    public static String HASHMAP_KEY_APPPATH = "r";
    public static String HASHMAP_KEY_SYSTEM = "t";
    public static String HASHMAP_KEY_DISABLED = "y";
    public static String HASHMAP_KEY_ICON = "u";
    public static String HASHMAP_KEY_INSTALLDATE = "i";
    public static String HASHMAP_KEY_FILESIZE = "o";
    public static final int START_ACTIVITY_KEY_UNINSTALL = 74;
}
