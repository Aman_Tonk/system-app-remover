package com.system.appremove.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class PoJo {
    public static int SORT_TYPE = 4;
    public static final int SORT_A2Z = 4;
    public static final int SORT_Z2A = 5;
    public static final int SORT_DATE = 6;
    public static final int SORT_SIZE = 7;

    private static ArrayList<HashMap<String, Object>> listdata;

    public static ArrayList<HashMap<String, Object>> getListdata() {
        return listdata;
    }

    public static void setListdata(ArrayList<HashMap<String, Object>> listdata) {
        PoJo.listdata = listdata;
        Collections.sort(PoJo.listdata, new MapComparator());
    }


    static class MapComparator implements Comparator<HashMap<String, Object>> {


        @Override
        public int compare(HashMap<String, Object> first, HashMap<String, Object> second) {
            int returnint = 0;
            String firstValue, secondValue;
            switch (SORT_TYPE) {
                case SORT_A2Z:
                    firstValue = first.get(Keys.HASHMAP_KEY_APPNAME).toString();
                    secondValue = second.get(Keys.HASHMAP_KEY_APPNAME).toString();
                    returnint = firstValue.compareTo(secondValue);
                    break;
                case SORT_Z2A:
                    firstValue = first.get(Keys.HASHMAP_KEY_APPNAME).toString();
                    secondValue = second.get(Keys.HASHMAP_KEY_APPNAME).toString();
                    returnint = secondValue.compareTo(firstValue);
                    break;
                case SORT_DATE:
                    firstValue = first.get(Keys.HASHMAP_KEY_INSTALLDATE).toString();
                    secondValue = second.get(Keys.HASHMAP_KEY_INSTALLDATE).toString();
                    returnint = firstValue.compareTo(secondValue);
                    break;
                case SORT_SIZE:
                    firstValue = first.get(Keys.HASHMAP_KEY_FILESIZE).toString();
                    secondValue = second.get(Keys.HASHMAP_KEY_FILESIZE).toString();
                    returnint = firstValue.compareTo(secondValue);
                    break;

            }
            return returnint;
        }

    }
}
