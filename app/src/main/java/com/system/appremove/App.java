package com.system.appremove;

import android.app.Application;

import com.stericson.RootTools.RootTools;
import com.system.appremove.Tool.Logs;
import com.system.appremove.Tool.Root;

/**
 * Created by Aman on 8/31/2015.
 */
public class App extends Application {
    private static boolean isRooted = false;

    @Override
    public void onCreate() {
        super.onCreate();
        RootTools.debugMode = BuildConfig.DEBUG;

        if (RootTools.isAccessGiven()) {
            isRooted = true;
            Logs.d("Root Available");
        } else {
            Logs.d("Root Not Available invoking Root Access");
            isRooted = Root.InvokeRoot();
        }
    }

    public static boolean isRooted() {
        return isRooted;
    }
}
