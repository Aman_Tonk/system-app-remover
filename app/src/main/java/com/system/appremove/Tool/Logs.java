package com.system.appremove.Tool;

import android.util.Log;

import com.system.appremove.BuildConfig;


/**
 * Created by Aman on 8/31/2015.
 */
public class Logs {
    public static void d(String msg) {
        if (BuildConfig.DEBUG)
            Log.d("Aman", msg);
    }
}
