package com.system.appremove.Tool;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.stericson.RootShell.execution.Command;
import com.stericson.RootTools.RootTools;
import com.system.appremove.App;
import com.system.appremove.activites.Appinfo;
import com.system.appremove.activites.MainActivity;
import com.system.appremove.data.Keys;
import com.system.appremove.data.PoJo;

/**
 * Created by Aman on 8/31/2015.
 */
public class Tools {
    public final Context context;

    public Tools(Context context) {
        this.context = context;
    }

    public void Share(int Position) {
        String Package = PoJo.getListdata().get(Position)
                .get(Keys.HASHMAP_KEY_PACKAGE).toString();
        String path = PoJo.getListdata().get(Position)
                .get(Keys.HASHMAP_KEY_APPPATH).toString();
        Share.app(
                context,
                Package,
                path,
                true,
                PoJo.getListdata().get(Position)
                        .get(Keys.HASHMAP_KEY_APPNAME).toString());
    }

    public void backup(int Position) {
        String Package2 = PoJo.getListdata().get(Position)
                .get(Keys.HASHMAP_KEY_PACKAGE).toString();
        String path2 = PoJo.getListdata().get(Position)
                .get(Keys.HASHMAP_KEY_APPPATH).toString();
        Share.app(context, Package2, path2,
                false, PoJo.getListdata().get(Position)
                        .get(Keys.HASHMAP_KEY_APPNAME).toString());
    }

    public void Uninstall(final int Position) {
        if (App.isRooted())
            new AlertDialog.Builder(context).setTitle("Sure?").setMessage("Are You Sure You Want to Uninstall " + PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_APPNAME) + "?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    new UninstallTask(Position).execute();
                }
            }).setNegativeButton("No", null).show();
        else if (!((boolean) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_SYSTEM))) {
            Toast.makeText(context, "Root Not Available, Trying Native Method", Toast.LENGTH_SHORT).show();
            Intent i2 = new Intent(
                    Intent.ACTION_DELETE,
                    Uri.parse("package:"
                            + PoJo.getListdata()
                            .get(Position)
                            .get(Keys.HASHMAP_KEY_PACKAGE)));
            i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i2);
        }else{
            Toast.makeText(context, "Oops! Cannot Uninstall System App Without Root Permission.", Toast.LENGTH_SHORT).show();
        }
    }

    public void EnableDisable(final int Position) {
        if(!App.isRooted()){
            Toast.makeText(context,"Cannot Disable app on a Non-Rooted phone. Root your Phone First",Toast.LENGTH_LONG).show();
            return;
        }
        final boolean disabled = (boolean) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_DISABLED);
        String message;
        if (disabled)
            message = "Enable";
        else
            message = "Disable";
        new AlertDialog.Builder(context).setTitle("Sure?").setMessage("Are You Sure You Want to " + message + " " + PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_APPNAME) + "?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new EnableDisableTask(Position, disabled).execute();
            }
        }).setNegativeButton("No", null).show();
    }

    public void Start(int Position) {
        try {
            Intent i1 = context.getPackageManager()
                    .getLaunchIntentForPackage(
                            PoJo.getListdata()
                                    .get(Position)
                                    .get(Keys.HASHMAP_KEY_PACKAGE).toString());
            i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i1);
        } catch (Exception e) {
            Toast.makeText(context,
                    "Cannot Open this App. " +
                            "No Launch Class Found",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void googleplay(int Position) {
        try {
            context.startActivity(new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id="
                            + PoJo.getListdata()
                            .get(Position)
                            .get(Keys.HASHMAP_KEY_PACKAGE))));

        } catch (Exception e) {
            context.startActivity(new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id="
                            + PoJo.getListdata()
                            .get(Position)
                            .get(Keys.HASHMAP_KEY_PACKAGE))));
        }
    }

    public void Shortcut(int Position) {
        try {
            Intent intent = context.getPackageManager()
                    .getLaunchIntentForPackage(PoJo
                            .getListdata().get(Position)
                            .get(Keys.HASHMAP_KEY_PACKAGE).toString());
            if (null != intent) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Intent addIntent = new Intent();
                addIntent.putExtra(
                        Intent.EXTRA_SHORTCUT_INTENT,
                        intent);
                addIntent
                        .putExtra(
                                Intent.EXTRA_SHORTCUT_NAME,
                                PoJo.getListdata()
                                        .get(Position)
                                        .get(Keys.HASHMAP_KEY_APPNAME).toString());
                addIntent
                        .putExtra(
                                Intent.EXTRA_SHORTCUT_ICON,
                                ((BitmapDrawable) PoJo
                                        .getListdata().get(
                                                Position).get(Keys.HASHMAP_KEY_ICON))
                                        .getBitmap());
                addIntent
                        .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                context.getApplicationContext().sendBroadcast(
                        addIntent);
                Toast.makeText(
                        context.getApplicationContext(),
                        "Shortcut Created on HomeScreen",
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(
                        context.getApplicationContext(),
                        "Unable to Create Shortcut Because This app is not An Launch App.",
                        Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            Toast.makeText(context.getApplicationContext(),
                    "Unable to Create Shortcut.",
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void Moreinfo(int Position) {
        try {
            Intent intent = new Intent(
                    android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:"
                    + PoJo.getListdata().get(Position)
                    .get(Keys.HASHMAP_KEY_PACKAGE)));
            context.startActivity(intent);

        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(
                    android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            context.startActivity(intent);

        }
    }

    class UninstallTask extends AsyncTask<Void, Void, Void> {
        int Position;
        boolean isDone = false;
        ProgressDialog progressDialog;

        UninstallTask(int Position) {
            this.Position = Position;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Uninstalling.. Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            String cmd, P_name = (String) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_PACKAGE);
            Logs.d("Uninstalling " + P_name);

            if ((boolean) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_SYSTEM)) {
                String App_Path = (String) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_APPPATH);
                String cmd2deldir = "/data/data/" + P_name;
                RootTools.deleteFileOrDirectory(cmd2deldir, true);
                cmd = "rm -r " + App_Path;
                RootTools.remount(App_Path, "RW");
            } else {

                cmd = "pm uninstall " + P_name;
            }

            try {
                Command command = new Command(0, cmd) {
                    @Override
                    public void commandOutput(int id, String line) {
                        Logs.d(line);
                        super.commandOutput(id, line);
                    }

                    @Override
                    public void commandTerminated(int id, String reason) {
                        Logs.d("Command Terminated - " + reason);
                        isDone = false;
                        super.commandTerminated(id, reason);
                    }

                    @Override
                    public void commandCompleted(int id, int exitcode) {
                        Logs.d("Command Completed , Exit Code = " + exitcode);
                        isDone = exitcode == 0;
                        super.commandCompleted(id, exitcode);
                    }
                };
                RootTools.getShell(true).add(command);
            } catch (Exception e) {
                e.printStackTrace();
                Logs.d("Fuck Exception - " + e.toString());
                isDone = false;
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (isDone) {
                Toast.makeText(context, "App Uninstalled", Toast.LENGTH_SHORT).show();
                if (context instanceof Appinfo) {
                    ((Appinfo) context).finish();
                }
                MainActivity.RefreshList();
            } else {
                Toast.makeText(context, "Unable to Uninstall App", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(aVoid);
        }
    }

    class EnableDisableTask extends AsyncTask<Void, Void, Void> {
        int Position;
        boolean isDone = false;
        final boolean disabled;
        ProgressDialog progressDialog;

        EnableDisableTask(int Position, boolean disabled) {
            this.Position = Position;
            this.disabled = disabled;
        }

        @Override
        protected void onPreExecute() {
            String msg;
            if (disabled)
                msg = "Enabling";
            else
                msg = "Disabling";
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(msg + ".. Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            String cmd, P_name = (String) PoJo.getListdata().get(Position).get(Keys.HASHMAP_KEY_PACKAGE);
            Logs.d("Freezing " + P_name);
            if (disabled) {
                cmd = "pm enable " + P_name;
            } else {
                cmd = "pm disable " + P_name;
            }

            try {
                Command command = new Command(0, cmd) {
                    @Override
                    public void commandOutput(int id, String line) {
                        Logs.d(line);
                        super.commandOutput(id, line);
                    }

                    @Override
                    public void commandTerminated(int id, String reason) {
                        Logs.d("Command Terminated - " + reason);
                        isDone = false;
                        super.commandTerminated(id, reason);
                    }

                    @Override
                    public void commandCompleted(int id, int exitcode) {
                        Logs.d("Command Completed , Exit Code = " + exitcode);
                        isDone = exitcode == 0;
                        super.commandCompleted(id, exitcode);
                    }
                };
                RootTools.getShell(true).add(command);
            } catch (Exception e) {
                e.printStackTrace();
                Logs.d("Fuck Exception - " + e.toString());
                isDone = false;
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String msg;
            if (disabled)
                msg = "Enabled";
            else
                msg = "Disabled";
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (isDone) {
                Toast.makeText(context, "App " + msg, Toast.LENGTH_SHORT).show();
                if (context instanceof Appinfo) {
                    ((Appinfo) context).finish();
                }
                MainActivity.RefreshList();
            } else {
                Toast.makeText(context, "Error : App is Not " + msg, Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(aVoid);
        }
    }
}
