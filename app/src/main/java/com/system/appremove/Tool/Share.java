package com.system.appremove.Tool;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.system.appremove.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Share {
	protected static String path = "";
	private static File apk;
	private static Context context;
	private static String packaegename;
	public static final int FLAG_SYSTEM_APP = 0;
	public static final int FLAG_NORMAL_APP = 1;
	public static boolean istemp;
	private static String appname;
	

	public static void app(final Context currentclasscontext,
			final String packagename, final String Path, boolean istemp,
			String appname) {
		Share.istemp = istemp;
		context = currentclasscontext;
		packaegename = packagename;
		Share.appname = appname;
		apk = new File(Path);
		if (apk.exists()) {
			new ShareApp().execute();
		}
	}

	public static class ShareApp extends AsyncTask<Void, Void, Void> {
		FileInputStream inputStream;
		FileOutputStream outputStream;
		File apk2;
		boolean isDone = false;
		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			progress = new ProgressDialog(context);
			if (istemp)
				progress.setMessage("Please Wait...");
			else
				progress.setMessage("Backup in Progress...\nPlease Wait!");
			progress.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				inputStream = new FileInputStream(apk);
				String path = Environment.getExternalStorageDirectory()
						.toString();
				File dir = null;
				if (istemp) {
					dir = new File(path, "/"
							+ context.getResources().getString(
									R.string.app_name) + "/.temp");
				} else {
					dir = new File(path, "/"
							+ context.getResources().getString(
									R.string.app_name) + "/Backups");
				}
				if (!dir.exists())
					dir.mkdirs();
				if (istemp)
					apk2 = new File(dir.getAbsolutePath(), packaegename
							+ ".apk");
				else
					apk2 = new File(dir.getAbsolutePath(), packaegename
							+ System.currentTimeMillis() + ".apk");
				if (apk2.exists() && istemp) {
					apk2.delete();
				}
				apk2.createNewFile();
				outputStream = new FileOutputStream(apk2);
				byte[] buffer = new byte[1024];
				int read;
				while ((read = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, read);
				}
				inputStream.close();
				inputStream = null;
				outputStream.flush();
				outputStream.close();
				outputStream = null;
				isDone = true;
			} catch (Exception e) {
				Log.d("Aman", "Unable to copy file");
				e.printStackTrace();
				isDone = false;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (progress.isShowing())
				progress.dismiss();
			if (isDone && istemp) {
				Intent i1 = new Intent(Intent.ACTION_SEND);
				i1.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(apk2));
				i1.setType("application/vnd.android.package-archive");
				i1.setType("text/plain");
				i1.putExtra(android.content.Intent.EXTRA_SUBJECT, context
						.getResources().getString(R.string.app_name) + ":-\n");
				i1.putExtra(
						android.content.Intent.EXTRA_TEXT,
						"Hey, I'm Using '"
								+ appname
								+ "' Download this app From https://play.google.com/store/apps/details?id="
								+ packaegename
								+ "\n\n(Shared via "
								+ context.getResources().getString(
										R.string.app_name)
								+ ")\n Get "
								+ context.getResources().getString(
										R.string.app_name)
								+ " From https://play.google.com/store/apps/details?id="
								+ context.getPackageName());
				context.startActivity(i1);
			} else {
				Log.d("AMAN", "Exception While Share");
			}
			if (!istemp && isDone)
				Toast.makeText(context,
						"apk Backup saved to " + apk2.getAbsolutePath(),
						Toast.LENGTH_SHORT).show();
			super.onPostExecute(result);
		}
	}

	public static long getfilesize(String Packagename, String Path) {
		apk = new File(Path);
		if (apk.exists())
			return apk.length();
		else
			return 1;
	}
}