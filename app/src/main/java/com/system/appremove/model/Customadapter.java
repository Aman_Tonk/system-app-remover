package com.system.appremove.model;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.system.appremove.R;
import com.system.appremove.Tool.Tools;
import com.system.appremove.activites.Appinfo;
import com.system.appremove.data.Keys;
import com.system.appremove.data.PoJo;


public class Customadapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private Tools tools;

    public Customadapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        tools = new Tools(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return PoJo.getListdata().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.listitem, null);
            holder.appicon = (ImageView) convertView
                    .findViewById(R.id.imageView1);
            holder.AppName = (TextView) convertView
                    .findViewById(R.id.textView_appname);
            holder.version = (TextView) convertView
                    .findViewById(R.id.textView_version);
            holder.share = (TextView) convertView
                    .findViewById(R.id.txtbtn_share);
            holder.cardView = (CardView)convertView.findViewById(R.id.cardview);
            holder.backup = (TextView) convertView.findViewById(R.id.txtbtn_backup);
            holder.more = (TextView) convertView.findViewById(R.id.txtbtn_more);
            holder.share.setFocusable(false);
            holder.share.setClickable(false);
            holder.backup.setFocusable(false);
            holder.backup.setClickable(false);
            holder.more.setFocusable(false);
            holder.more.setClickable(false);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.appicon.setImageDrawable((Drawable) PoJo.getListdata().get(position).get(Keys.HASHMAP_KEY_ICON));
        holder.AppName.setText(PoJo.getListdata().get(position)
                .get(Keys.HASHMAP_KEY_APPNAME).toString());
        holder.version.setText("v"
                + PoJo.getListdata().get(position)
                .get(Keys.HASHMAP_KEY_VERSION));
        holder.share.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tools.Share(position);
            }
        });
        holder.backup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tools.backup(position);
            }
        });
        holder.more.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tools.Uninstall(position);
            }
        });
        holder.cardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(context, Appinfo.class);
                i1.putExtra(Appinfo.INTENT_KEY_POSITION, position);
                context.startActivity(i1);
            }
        });
        return convertView;
    }

    public static class ViewHolder {
        TextView AppName, version;
        ImageView appicon;
        TextView share, backup, more;
        CardView cardView;
    }
}
